let users = {};
let loggedInUser = "";
function ATM(cmd, user, amount) {
    let msg = "";
    switch (cmd) {
        case "login":
            msg = loginMsg(user);
            loggedInUser = user;
            msg += userValidation(user);
            return msg;
        case "logout":
            msg = logoutMsg(loggedInUser);
            loggedInUser = "";
            return msg;
        case "transeferred":
            return transeferred(user, amount);

        case "deposit":
            return deposit(amount);
        case "withdraw":

            return withdraw(amount);
    }

}

function userValidation(user) {
    try {
        let msg = "";
        if (!users[user]) {
            users[user] = { amount: 0 };
            msg = balanceMsg(users[user].amount);
        } else {
            msg = balanceMsg(users[user].amount);
            if ((users[loggedInUser].borrower)) {
                for (let key of Object.keys(users[loggedInUser].borrower)) {
                    let ele = users[key].borrower[loggedInUser];
                    if (ele.amount < 0) {
                        msg += `\nOwed $${Math.abs(ele.amount)} from ${key}`;
                    }

                }
            }


        }
        return msg;
    } catch (err) {
        return interServerError();
    }

}

function transeferred(user, transerredAmount) {
    try {
        let msg = "";
        if (users[user]) {
            if (!users[loggedInUser].borrower) {
                users[loggedInUser].borrower = {};
                users[loggedInUser].borrower[user] = {};
            }
            if (!users[user].borrower) {
                users[user].borrower = {};
                users[user].borrower[loggedInUser] = {};
            } else if (users[user].borrower && !users[user].borrower[loggedInUser]) {
                users[user].borrower[loggedInUser] = {};
            }
            if (users[user].borrower[loggedInUser].amount) {
                // deduct amount from pending amount if user transer amount to other user
                if (Math.abs(users[user].borrower[loggedInUser].amount) > transerredAmount) {
                    users[user].borrower[loggedInUser].amount += transerredAmount;
                    msg += balanceMsg(users[loggedInUser].amount) +
                        owedMsg(Math.abs(users[user].borrower[loggedInUser].amount), user)
                } else {
                    let reminder = Math.abs(users[user].borrower[loggedInUser].amount + transerredAmount);
                    users[loggedInUser].amount -= reminder
                    users[user].borrower[loggedInUser].amount = 0;
                    msg += balanceMsg(users[loggedInUser].amount) + transerMsg(reminder, user);
                }

            } else {
                // deduct amount from loggedin user if no due pending
                users[user].borrower[loggedInUser].amount = 0;
                let reminderAmount = 0;
                if (users[loggedInUser].amount <= transerredAmount) {
                    users[user].amount += users[loggedInUser].amount;

                    reminderAmount = users[loggedInUser].amount - transerredAmount;
                    msg = transerMsg(users[loggedInUser].amount, user) + balanceMsg(0);

                    if (users[loggedInUser].amount - transerredAmount < 0) {
                        msg += owedMsg(transerredAmount - users[loggedInUser].amount, user);
                    }
                    users[loggedInUser].amount = 0;
                } else {
                    users[user].amount += transerredAmount;
                    users[loggedInUser].amount = users[loggedInUser].amount - transerredAmount;
                    msg = transerMsg(transerredAmount, user) + balanceMsg(users[loggedInUser].amount)
                }



                if (reminderAmount < 0) {
                    users[loggedInUser].borrower[user].amount = reminderAmount;
                    users[loggedInUser].amount = 0;
                }
            }
            return msg;
        } else {
            return "Account not exists."
        }
    } catch (err) {
        return interServerError();

    }

}
function deposit(amount) {
    try {
        users[loggedInUser].amount += amount;
        let msg = "";
        if (users[loggedInUser].borrower) {
            for (let key of Object.keys(users[loggedInUser].borrower)) {
                ele = users[loggedInUser].borrower[key];
                if (ele.amount) {


                    if (Math.abs(ele.amount) <= users[loggedInUser].amount) {
                        users[loggedInUser].amount -= Math.abs(ele.amount);
                        msg += transerMsg(Math.abs(ele.amount), key);
                        users[key].amount += Math.abs(ele.amount);
                        ele.amount = 0;


                    } else {
                        ele.amount += users[loggedInUser].amount;
                        users[key].amount += users[loggedInUser].amount;
                        users[loggedInUser].amount = 0;
                        msg += transerMsg(Math.abs(amount), key);
                        msg += owedMsg(Math.abs(ele.amount), key);
                    }

                }
            }
        } else {
            msg = balanceMsg(users[loggedInUser].amount);
        }

        return msg;
    } catch (err) {
        return interServerError();
    }

}

function withdraw(withDrawAmount) {
    try {
        if (users[loggedInUser].amount < withDrawAmount) {
            return "Insuficient ballance."
        } else {
            users[loggedInUser].amount -= withDrawAmount;
            msg = withdrawMsg(withDrawAmount) + balanceMsg(users[loggedInUser].amount);
            return msg;
        }
    } catch (err) {
        return interServerError()
    }


}

function balanceMsg(amount) {
    return `Your balance is $${amount}\n`;
}
function transerMsg(amount, user) {
    return `Transferred $${Math.abs(amount)} to ${user}\n`;
}

function owedMsg(amount, user) {
    return `Owed $${Math.abs(amount)} to ${user}\n`;
}
function loginMsg(user) {
    return `Hello, ${user}!\n`;
}

function logoutMsg(user) {
    return `Goodbye ${user}!\n`
}
function withdrawMsg(amount) {
    return `You have withdraw $${amount}\n`
}

function interServerError() {
    return "Opps!! Some thing went wrong. Try after some time"
}


console.log(ATM("login", "Ram"));
console.log(ATM("deposit", "", 100));
console.log(ATM("deposit", "", 10));
console.log(ATM("logout", "Ram"));
console.log(ATM("login", "Komal"));
console.log(ATM("deposit", '', 10));
console.log(ATM("transeferred", "Ram1", 155));
console.log(ATM("deposit", '', 20));
console.log(ATM("login", "Ram"));
console.log(ATM("transeferred", "Komal", 150));
console.log(ATM("withdraw", '', 8))

console.log(ATM("login", "Aman"));
console.log(ATM("transeferred", "Ram", 130));
console.log(ATM("withdraw", '', 8));
console.log(ATM("login", "Ram"));
console.log(ATM("transeferred", "Aman", 10));

